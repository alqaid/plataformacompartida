<?php

/**
* Devuelve los post de auzalan en formato galeria de feb-mar 2018 (Angel Alcaide)
* @param array Posts
* @return string HTML con los posts formateados
* llamada: 
* 			[plg_auzalan_galeria estilo="estilox" num_columnas="3" num_nodos_max="20" buscar="novedades"]
* ejemplos:
* http://icolegia.ihabiteformacion.es/auzalan-ejemplos-de-diseno/
*/


/*                                     ESTILO 6       */
function auzalan_template_galeria_v1_estilo6( $array, $atts) {
	$error= '';
	$contenedorIni='<!-- -------------------------------- AUZALAN VERSION: ' 
		. __AUZ_VERSION__ .' estilo6 --><ul class="auz_gallery_v1">';
	$contenedorFin='</ul><!-- --------------------------------< FIN AUZALAN - -->';
	$num_columnas='33.333%';
	if(is_numeric($atts["num_columnas"])) {
			$valor=100/ $atts["num_columnas"];
			$num_columnas=$valor . '%';
	}
	$galeria = "";
	/* IMAGEN */
	for($i=0;$i<count($array);$i++){
	if (!empty($array[$i]['id'])) {
    
	
		$id='auz_id_' . $array[$i]['id'];
		
		$id_texto_reducido='auz_id_txt_red_' . $array[$i]['id'];
		$id_enlace='auz_id_enlace_' . $array[$i]['id'] ;
		
		$imagen='';
		$categoriaIcono = ($array[$i]['CategoriaPadre'] != Null) ? $categoriaIcono=$array[$i]['CategoriaPadre'] : '56';	
		$imagen = '<img src="' . __AUZALAN_WEB_DIRECTORIO__ .$categoriaIcono.'.png" width="78px"/>';
		$imagenicono =$imagen;
			
		if ($array[$i]['Imagen'] != Null){
				$imagen='<img src="' . $array[$i]['Imagen'] . '" />';
		}
		
		/* TITULOS*/
		$titulo=$array[$i]['Titulo'];
		
		/* enlaces */
		$enlace_izquierda='';
		
		$enlace_derecha='';
		if ($array[$i]['Url']!=Null){ 
				$enlace_derecha='<a href="' . $array[$i]['Url'] .'" target="_blank">ir a publicación</a>';
				$titulo='<a href="'.__AUZALAN_POST_VIEWER_PAGE__.'?postid='. $array[$i]['id'].'">'. $titulo . '</a>';
		}
		
		/* TEXTOS */
		/*elimino ademas strip_tags etiquetas html, ver como puedo permitir algunas en http://php.net/manual/es/function.strip-tags.php*/
		 
		$textoampliado='';
		$contenido='';
		if ($array[$i]['Contenido']!=Null){ 
				 $contenido=trim($array[$i]['Contenido']);
		}
		
			if ($contenido != ''){
			    // no hay texto reducido $textoreducido= strip_tags($contenido);    /* quito ETIQUETAS HTML AL TEXTO REDUCIDO*/
				$textoampliado= $contenido;
				
				
			}
		
		/* ADJUNTO */
		$adjunto='';
		if ($array[$i]['Adjunto'] != Null){
				$adjunto='<img src="' . __AUZALAN_WEB_DIRECTORIO__ .'adjunto.png" />&nbsp;<a href="' . $array[$i]['Adjunto'] . '" target="_blank"/>descargar adjunto</a>';
		}
		$textoampliado .= '<br>' . $adjunto . '<br>';
		
		
		/* UNIR EL ESTILO ENTERO */
		$galeria .= '
		<li>
		<div class="auz_box">
			<figure>' . $imagen . '		</figure>
			<h3>' . $titulo . '</h3>
		 
			<section class="auz_textoampliado" id="'. $id .'"><p>' . $textoampliado . '</p></section>
			<figure class="auz_enlace_centro">' . $imagenicono . '</section>
			<section class="auz_enlace_izquierda">&nbsp;</section>
			<section class="auz_enlace_derecha">' . $enlace_derecha . '</section>
			
			</div>
		</li>
		';
	}else{
		$error= '<!-- sin conexion --> ';
	}
	}

	//-------------  hoja de estilos---------------------------------------------------------------------
	/* agregamos despues el NUMERO DE COLUMNAS para que tenga prioridad */
	$estilo =  '<style>.auz_gallery_v1 li{width:' . $num_columnas . ';}</style>'; 
	$estilo .= '<link rel="stylesheet" href="'.__AUZALAN_DIR__.'css/auzalan-template-galeriav1-estilo6.css" type="text/css">';
 
	
	$script = '';
    //------------- scripts -----------------------------------------------------------------------------

	if ($error == '' ){
		$devuelvo =$estilo . $contenedorIni .' ' . $galeria . ' ' . $contenedorFin .' ' . $script . ' ';
	}else	{
		$devuelvo =$error;
	}
return ($devuelvo);
}
