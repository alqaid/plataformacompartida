window.onload = function() {
  //funciones a ejecutar al cargar plugin: auzalan-template-galeriav1
   console.log('inicio plugin auzalan: auzalan-template-galeriav1');
   auz_template_galeriav1_ocultar_textos_largos_y_dividir_texto_corto();   
};

function auz_template_galeriav1_ocultar_textos_largos_y_dividir_texto_corto(){
  /*    la lanzamos en la carga del plugin, 
    ocultamos todos los textos AMPLIADOS    
*/ 
    var x = document.querySelectorAll(".auz_gallery_v1 li section.auz_textoampliado"); 
    var i; 
    for (i = 0; i < x.length; i++) { 
        x[i].style.display = "none"; 
    }  
}

function auz_template_galeriav1_mostrar_ocultar_texto(id,id_texto_reducido,id_enlace){
/*    Al hacer clic en los enlaces VER MAS o VER MENOS
    alternativamente OCULTA texto reducido y MUESTRA el texto ampliado 
    también cambia el nombre del enlace de VER MÁS  a VER MENOS
     event.preventDefault()  evita que se mueva la página, el FOCUS  se queda en la zona en la que estamos.
*/  
  if (document.getElementById(id).style.display=="none"){
    document.getElementById(id).style.display = "";
    document.getElementById(id_texto_reducido).style.display = "none";
    document.getElementById(id_enlace).innerHTML = "ver menos";
    event.preventDefault();   
 }else{
    document.getElementById(id).style.display = "none"; 
    document.getElementById(id_texto_reducido).style.display = "";
    document.getElementById(id_enlace).innerHTML = "ver más";
    event.preventDefault(); 
 }  
}
function auz_template_galeriav1_mostrar_ocultar_texto5(id,id_texto_reducido,id_enlace,vermas,vermenos){
/*    Al hacer clic en los enlaces VER MAS o VER MENOS
    alternativamente OCULTA texto reducido y MUESTRA el texto ampliado 
    también cambia el nombre del enlace de VER MÁS  a VER MENOS
     event.preventDefault()  evita que se mueva la página, el FOCUS  se queda en la zona en la que estamos.
*/  
  if (document.getElementById(id).style.display=="none"){
    document.getElementById(id).style.display = "";
    document.getElementById(id_texto_reducido).style.display = "none";
    document.getElementById(id_enlace).innerHTML = vermenos;
    event.preventDefault();   
 }else{
    document.getElementById(id).style.display = "none"; 
    document.getElementById(id_texto_reducido).style.display = "";
    document.getElementById(id_enlace).innerHTML = vermas;
    event.preventDefault(); 
 }  
}
