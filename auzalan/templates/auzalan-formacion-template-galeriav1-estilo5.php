<?php

/**
* Devuelve los post de auzalan en formato galeria de feb-mar 2018 (Angel Alcaide)
* @param array Posts
* @return string HTML con los posts formateados
* llamada: 
* 				[plg_auzalan_galeria bdd="formacion" estilo="estilox" num_columnas="3" num_nodos_max="20"]
* ejemplos:
* http://icolegia.ihabiteformacion.es/auzalan-ejemplos-de-diseno/
*
*   MISMA HOJA ESTILOS QUE ESTILO4
*/


/*        formacion                             ESTILO 5        */
function auzalan_formacion_template_galeria_v1_estilo5( $array, $atts) {

	$error= '';
	$contenedorIni='<!-- -------------------------------- AUZALAN VERSION: ' 
		. __AUZ_VERSION__ .' --><ul class="auz_gallery_v1">';
	$contenedorFin='</ul><!-- --------------------------------< FIN AUZALAN - -->';
	 
	$galeria = "";
	/* IMAGEN */
	
	
	for($i=0;$i<count($array);$i++){
     $mostrar=1;
	 if (!empty($array[$i]['idPb'])) {
	 
	 if(is_numeric($atts["colegio_excluye"])) {
		  if($atts["colegio_excluye"]==$array[$i]['CidO']) {
			$mostrar=0;			
		  }	
	 }
	 
	 if(is_numeric($atts["colegio"])) {
		  if($atts["colegio"]==$array[$i]['CidO']) {
			$mostrar=1;
			}else{
			$mostrar=0;
		  }	
	 }
 
     if ( $mostrar==1){
 
		$imagen='';
		$imagenicono = '<img src="' . __AUZALAN_DIR__ . '/css/icono.png" width="78px"/>';

		if ($array[$i]['imagen'] != Null){
				$imagen='<figure><img src="' . $array[$i]['imagen'] . '" /></figure>';
		}
		
		/* TITULOS*/
		$titulo=$array[$i]['titulo'];
		$titulo='<a href="'.__FORMACION_AUZALAN_POST_VIEWER_PAGE__.'?postid='. $array[$i]['idPb'].'">'. $titulo . '</a>';
		/* enlaces */
		$enlace_izquierda='';
		
		$enlace_derecha='';
		if ($array[$i]['url']!=Null){ 
				$enlace_derecha='<a href="' . $array[$i]['url'] .'" target="_blank">inscripción</a>';				
		}
		$enlace_izquierda='<a href="'.__FORMACION_AUZALAN_POST_VIEWER_PAGE__.'?postid='. $array[$i]['idPb'].'">ver más</a>';
		/* TEXTOS */
		$contenido='';
		if ($array[$i]['descripcion']!=Null){ 
				 $contenido=trim($array[$i]['descripcion']);
		}
		
		$textoampliado = $contenido .'<br>';
		
		 
		/* UNIR EL ESTILO ENTERO */
		$galeria .= '
		<li>
		<div class="auz_box">
			' . $imagen . '	
			<div class="contenido">
				<div class="titulo"> 
					<div class="tituloicono">' . $imagenicono . '</div>
					<div class="titulotexto"><h3>' . $titulo . '</h3></div>
				</div>
				<section class="textoampliado">' . $textoampliado . '</section>		
				<section class="auz_enlace_izquierda">' . $enlace_izquierda . '</section>				
				<section class="auz_enlace_derecha">' . $enlace_derecha . '</section>
			</div>
		</div>
		</li>
		';
	} }else{
		$error= '<!-- sin conexion --> ';
	} 
	}

	//-------------  hoja de estilos---------------------------------------------------------------------
	/* agregamos despues el NUMERO DE COLUMNAS para que tenga prioridad */
	$estilo = '<link rel="stylesheet" href="'.__AUZALAN_DIR__.'css/auzalan-template-galeriav1-estilo4.css" type="text/css">';
 
	
	$script = '';
    //------------- scripts -----------------------------------------------------------------------------

	if ($error == '' ){
		$devuelvo =$estilo . $contenedorIni .' ' . $galeria . ' ' . $contenedorFin .' ' . $script . ' ';
	}else	{
		$devuelvo =$error;
	}
return ($devuelvo);
}
