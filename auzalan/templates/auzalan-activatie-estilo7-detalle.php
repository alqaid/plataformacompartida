<?php

/**
* Devuelve los post de auzalan en formato galeria de feb-mar 2018 (Angel Alcaide)
* @param array Posts
* @return string HTML con los posts formateados
* llamada: 
* 				[plg_auzalan_galeria bdd="formacion" estilo="estilox" num_columnas="3" num_nodos_max="20"]
* ejemplos:
* http://icolegia.ihabiteformacion.es/auzalan-ejemplos-de-diseno/
*
*   ESTILO DE ALBACETE
*/


/*        activatie                             ESTILO 7  detalle      */
function auzalan_activatie_estilo7_detalle($arrayCurso, $atts, $post_id) {
 
	$error= '';
	$contenedorIni='<!-- -------------------------------- AUZALAN VERSION: ' 
		. __AUZ_VERSION__ .' --><ul class="auz_gallery_v1">';
	$contenedorFin='</ul><!-- --------------------------------< FIN AUZALAN - -->';
	 
	$galeria = "";
	/* IMAGEN */
	
	$array=$arrayCurso['curso'];
	for($i=0;$i<count($array);$i++){
    if (!empty($array[$i]['id'])) {
	if ( $array[$i]['id']==$post_id){
 
		$imagen='';
		 
		if ($array[$i]['imageninterior'] != Null){
				$imagen=$array[$i]['imageninterior'];
		}
		
		/* TITULOS*/
		$titulo=$array[$i]['nombre'];
		
		
		
		/* TEXTOS */
		$contenido='<b>' . mb_strtoupper($titulo) . '</b><br><br>';
		if ($array[$i]['descripcion']!=Null){ 
				 $contenido .=trim($array[$i]['descripcion']);
		}
		if ($array[$i]['lugar']!=Null){ $contenido .='<br><b>Lugar</b>: ' .  $array[$i]['lugar'] .'<br>';}
		if ($array[$i]['duracion']!=Null){ $contenido .='<br><b>Duración</b>: ' .  $array[$i]['duracion'] .'<br>';}
		if ($array[$i]['tipo']!=Null){ $contenido .='<br><b>Tipo</b>: ' .  $array[$i]['tipo'] .'<br>';}
		if ($array[$i]['modalidad']!=Null){ $contenido .='<br><b>Modalidad</b>: ' .  $array[$i]['modalidad'] .'<br>';}
		if ($array[$i]['precio_colegiado']!=Null){ $contenido .='<br><b>Precio Colegiado</b>: ' .  $array[$i]['precio_colegiado'] .'<br>';}
		if ($array[$i]['precio_nocolegiado']!=Null){ $contenido .='<br><b>Precio No Colegiado</b>: ' .  $array[$i]['precio_nocolegiado'] .'<br>';}
		if ($array[$i]['fecha_inicio']!=Null){ $contenido .='<br><b>Fecha Inicio</b>: ' .  $array[$i]['fecha_inicio'] .'<br>';}
		if ($array[$i]['fecha_fin']!=Null){ $contenido .='<br><b>Fecha Fin</b>: ' .  $array[$i]['fecha_fin'] .'<br>';}
		if ($array[$i]['horarios']!=Null){ $contenido .='<br><b>Horarios</b>: ' .  $array[$i]['horarios'] .'<br>';}
		
		foreach($array[$i]['areas'] as $arrayAreas){
			if ($arrayAreas['nombre']!=Null){    $contenido .='<b>    Area : </b>' . $arrayAreas['nombre'].'<br>' ;  }
		 }
		 
		$enlace='';
				if ($array[$i]['link']!=Null){ 
					$enlace='
					<div class="et_pb_button_wrapper"><a target="_blank" class="et_pb_button et_pb_promo_button" href="' . $array[$i]['link'] .
					'" data-icon="$">Inscripción</a></div>';				
			}
	/*	$adjunto='';
			if ($array[$i]['adjunto']!=Null){ 
				$adjunto='
				<div style="margin-top:5px" class="et_pb_button_wrapper"><a target="_blank"  class="et_pb_button et_pb_promo_button" href="' . $array[$i]['adjunto'] .
				'" data-icon="$">Descargar Adjunto</a></div>';				
		}*/
		
		
		$contenido=$contenido . '<br><br>' . $adjunto  . $enlace;	

		
		/* UNIR EL ESTILO ENTERO */
		$galeria .= '
		<link rel="stylesheet" href="http://new.aparejadoresalbacete.es/wp-content/plugins/auzalan/css/auzalan-formacion-template-galeriav1-estilo7-detalle.css" type="text/css">
<div class="et_pb_row et_pb_row_1 et_pb_gutters3">	

	<div class="et_pb_module et_pb_image et_pb_image_0">				
				<span class="et_pb_image_wrap has-box-shadow-overlay"><div class="box-shadow-overlay"></div><img  src="' . $imagen . '	
			" alt="" title=""  class="auzalan_mi_sombra" /></span>
	</div>
	<div class="et_pb_with_border et_pb_module et_pb_cta_0 et_hover_enabled et_pb_promo  et_pb_text_align_left et_pb_bg_layout_light et_pb_no_bg">' . $contenido . '</div>
</div>';
	} 
	}else{
		$error= '<!-- sin conexion --> ';
	 
	} // fin FOR
	}

	//-------------  hoja de estilos---------------------------------------------------------------------
	/* agregamos despues el NUMERO DE COLUMNAS para que tenga prioridad */
	$estilo = '<link rel="stylesheet" href="'.__AUZALAN_DIR__.'css/auzalan-template-galeriav1-estilo4.css" type="text/css">';
 
	
	$script = '';
    //------------- scripts -----------------------------------------------------------------------------



	if ($error == '' ){
		$devuelvo =$estilo . $contenedorIni .' ' . $galeria . ' ' . $contenedorFin .' ' . $script . ' ';
	}else	{
		$devuelvo =$error;
	}
return ($devuelvo);
}

function auzalan_template_galeria_v1_estilo7_3variascolumnas_detalle( $array, $atts) {
	$error= '';
	$version ='<!-- -------------------------------- AUZALAN VERSION: '. __AUZ_VERSION__ .' estilo7 -->';
	$content='';
	$content0cab='';
	$content0pie='';
	$content1='<center>';
	$content2='</center><h2>';
	$content3='</h2>	
					<div class="auz_texto_justificar">';
	$content4='';
	$content5='';
	$pie='<!-- --------------------------------< FIN AUZALAN - -->';
	
	
	
	for($i=0;$i<count($array);$i++){
		if (!empty($array[$i]['id'])) {			
			$imagen='';
			$title=$array[$i]['Titulo'];
			$texto='';
			
			if ($array[$i]['Contenido']!=Null){ 
				 $texto=trim($array[$i]['Contenido']);
				// $texto= strip_tags($texto);    /* quito ETIQUETAS HTML AL TEXTO REDUCIDO*/				
			}
			$imagen = '<img src="' . __AUZALAN_DIR__ . 'imagen.png"  width="400px" height="300px" class="auzalan_caja_sombra"/>';
			if ($array[$i]['Imagen'] != Null){
				$imagen='<img src="' . $array[$i]['Imagen'] . '" alt="" title=""  class="auzalan_caja_sombra"  >';
			}
			$enlace='';
				if ($array[$i]['Url']!=Null){ 
					/*$enlace='
					<div class="et_pb_button_wrapper"><a target="_blank" class="et_pb_button et_pb_promo_button" href="' . $array[$i]['Url'] .
					'" data-icon="$">Ir a publicación</a></div>';*/
				$enlace='<div class="auzleermas_caja_detalle">
									<a class="auzleermas" 
									href="'. $array[$i]['Url'].'" 
									data-icon="&#x45;" target="_blank">Ir a publicación</a>
									</div>';					
			}
			$adjunto='';
				if ($array[$i]['Adjunto']!=Null){ 
					/*$adjunto='
					<div style="margin-top:5px" class="et_pb_button_wrapper"><a target="_blank"  class="et_pb_button et_pb_promo_button" href="' . $array[$i]['Adjunto'] .
					'" data-icon="$">Descargar Adjunto</a></div>';	*/
					$adjunto='<div class="auzleermas_caja_detalle">
									<a class="auzleermas" 
									href="'. $array[$i]['Adjunto'].'" 
									data-icon="&#x45;"  target="_blank">Descargar Adjunto</a>
									</div>';		
					
			}
		
			
			
			
			$content =$content . $content1 . $imagen . $content2 . $title . $content3 . $texto . $content4 . $enlace . $adjunto . $content5;
		}
	}
	
	$estilo = ' <link rel="stylesheet" href="'.__AUZALAN_DIR__.'css/auzalan-template-galeriav1-estilo7-variascolumnas.css" type="text/css"> ';
	
	if ($error == '' ){
		$devuelvo =$version . $estilo . $content0cab . $content . $content0pie . $pie ;
	}else	{
		$devuelvo =$error;
	}
	
	
	
return ($devuelvo);
}