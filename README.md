# PLUGIN PARA WORDPRESS
#### AUZALAN  

* Versi�n 17.06.2024


* Web de Auzalan: http://www.auzalan.net

* REQUISITO IMAGENES:
proporcion de imagenes: 6x4 ejemplo: 600px X 400px  o  300px X 200px
----------------------------------------

#### CONFIGURACI�N
##### PARMETROS CONFIGURABLES DEL PLUGIN
#####  define(�CLAVE_USUARIO�,���..�);
#####  define(�AUZALAN_URL�,���..�);
#####  define(�FORMACION_AUZALAN_POST_VIEWER_PAGE�,AUZALAN_URL.���../�);
#####  define(�AUZALAN_FORMACION_TEXTO_NoResultados�,�En estos momentos no hay actividades en periodo de inscripcin.�);

 
#### DESCRIPCI�N

#####  CLAVE_USUARIO: Solicitar

#####  AUZALAN_URL: Direcci�n de la ubicaci�n de su plugin, ejemplo (https://susitioweb.es/)

#####  FORMACION_AUZALAN_POST_VIEWER_PAGE: Url a una p�gina que se ha de crear para ampliar informaci�n a un determinado post de Auzalan. ejemplo: AUZALAN_URL.�detalle_formacionauzalan�

#####  AUZALAN_FORMACION_TEXTO_NoResultados: Texto descriptivo si no existen actividades, cursos, noticias relaccionadas�

________________________________________

AUZALAN CMS
#### Nodos por 
* [plg_auzalan_galeria bdd="auzalan" estilo="estilo3" num_columnas="3" num_nodos="9" buscar="Sociocultural"]

#### NECESARIO CREAR PGINA O POST CON ESTE CDIGO
* [plg_auzalan_post_viewer]
* [plg_auzalan_post_viewer  estilo='estilo9AS']

* Tambi�n estilos: estilo1, estilo7, estilo9AS


#### Ejemplos de uso GESTOR DE CONTENIDOS categoria 6 ( NOTICIAS DE INTERES)

* [plg_auzalan_galeria bdd="auzalan" estilo="estilo3_2" num_columnas="3" num_nodos="30" cat_hijo="6"]

________________________________________
#### ----- ----- ----- ----- FORMACI�N AUZALAN ----- ----- ----- ----- 

#### NECESARIO CREAR P�GINA O POST CON ESTE C�DIGO para FORMACI�N
* [plg_auzalan_post_viewer_form]
* [plg_auzalan_post_viewer_form estilo='estilo7'] 
* Tambi�n estilos: estilo1, estilo7, estilo9AS

#### Par�metros:

* bdd='formacion'
* num_columnas='3' 
* num_nodos='3' 
* estilo='estilo1'
* colegio=2
* colegio_excluye="2"
* orden="DESC"  -- por defecto ASC

#### Ejemplos de uso FORMACI�N auzalan

* [plg_auzalan_galeria bdd=�formacion� num_columnas=�3' num_nodos=�3' estilo=�estilo1']
* [plg_auzalan_galeria bdd=�formacion� num_columnas=�3' num_nodos=�3' estilo=�estilo2']
* [plg_auzalan_galeria bdd=�formacion� num_columnas=�3' num_nodos=�3' estilo=�estilo3']
* [plg_auzalan_galeria bdd=�formacion� num_columnas=�3' num_nodos=�3' estilo=�estilo4']
* [plg_auzalan_galeria bdd=�formacion� num_columnas=�3' num_nodos=�3' estilo=�estilo5']

#### Ejemplos de uso FORMACION auzalan solo cursos de un COLEGIO (se necesita id del Colegio)

* [plg_auzalan_galeria bdd="formacion" num_columnas="4" num_nodos="" estilo="estilo7" colegio=2 orden="DESC"]

#### Ejemplos de uso FORMACION auzalan resto de cursos de OTROS Colegios (se necesita id del Colegio)

* [plg_auzalan_galeria bdd="formacion" num_columnas="4" num_nodos="" estilo="estilo7" colegio_excluye="2" orden="DESC"]

##### CURSOS PROPIOS: en una sola columna y se abre en p�gina nueva. (DE SU COLEGIO) 

[plg_auzalan_galeria bdd='formacion' estilo='estilo5' num_columnas='1' colegio='2' ]

##### CURSOS EXTERNOS: en una sola columna y se abre en p�gina nueva (EXCEPTO SU COLEGIO)

[plg_auzalan_galeria bdd='formacion' estilo='estilo5' num_columnas='1' colegio_excluye='2' ]


________________________________________
#### ----- ----- ----- -----    ACTIVATIE ----- ----- ----- ----- 

[plg_auzalan_galeria bdd="activatie" estilo="estilo7" num_columnas="" num_nodos=""]

####  P�gina de detalle DEBE SER:  
* [plg_auzalan_activatie_detalle]
* [plg_auzalan_activatie_detalle estilo='estilo9AS']

* Tambi�n estilos: estilo1, estilo7, estilo9AS

________________________________________
Estilos ver ejemplos: 
#### http://icolegia.ihabiteformacion.es/formacion-modo-galeria-v1-estilo1/
#### http://icolegia.ihabiteformacion.es/formacion-modo-galeria-v1-estilo2/
#### http://icolegia.ihabiteformacion.es/formacion-modo-galeria-v1-estilo3/
#### http://icolegia.ihabiteformacion.es/formacion-modo-galeria-v1-estilo4/
#### http://icolegia.ihabiteformacion.es/formacion-modo-galeria-v1-estilo5/

________________________________________
## VERSIONES
##### 2024.06.17 ( Agregar funcion de agenda para web de asturias plugin: [plg_auzalan_agenda cat_hijo="278"] )
##### 2024.03.22 ( Agregar im�gen por defecto )
##### 2024.03.19 ( estilo="estilo9AS"     portada de asturias - FORMACI�N, ACTIVATIE, GESTOR CONTENIDO AUZALAN)
##### 2022.09.16 ( estilo="estilo3_4"    para que el t�tulo y ver m�s sean el mismo enlace  se public� en la portada de ciudad real )
##### 2022.04.08 ( Arreglar estilo7 auzalan alguna clase interfer�a con las plantillas DIVI)
##### 2022.04.07 ( ORDEN por FECHA LIMITE INSCRIPCION ) Nuevo comando en el plugin [    orden="ASC"  orden="DESC"  ] 
##### 2022.04.07 ( Reparado estilo 7p  ) link no llevaba al detalle del curso.
