<?php

/**
* Devuelve los post de auzalan en formato galeria de feb-mar 2018 (Angel Alcaide)
* @param array Posts
* @return string HTML con los posts formateados
* llamada: 
* 			[plg_auzalan_galeria estilo="estilox" num_columnas="3" num_nodos_max="20" buscar="novedades"]
* ejemplos:
* http://icolegia.ihabiteformacion.es/auzalan-ejemplos-de-diseno/
*/


/*                                     ESTILO 6       */
function auzalan_template_galeria_v1_estilo7_detalle( $array, $atts) {
	$error= '';
	$version ='<!-- -------------------------------- AUZALAN VERSION: '. __AUZ_VERSION__ .' estilo7 -->';
	$content='';
	$content0cab='<div class="et_pb_row et_pb_row_1 et_pb_gutters3">';
	$content0pie='</div>';
	$content1='	
<div class="et_pb_row et_pb_row_1 et_pb_gutters3 et_pb_row_1-4_3-4">
	<div class="et_pb_column et_pb_column_1_4 et_pb_column_1  et_pb_css_mix_blend_mode_passthrough">
		<div class="et_pb_module et_pb_image et_pb_image_0">
			<span class="et_pb_image_wrap has-box-shadow-overlay">
				<div class="box-shadow-overlay"></div>';
	$content2='		
			</span>
		</div>		
	</div>		
	<div class="et_pb_column et_pb_column_3_4 et_pb_column_2  et_pb_css_mix_blend_mode_passthrough et-last-child">
		<div class="et_pb_with_border et_pb_module et_pb_cta_0 et_hover_enabled et_pb_promo  et_pb_text_align_left et_pb_bg_layout_light et_pb_no_bg">
				<div class="et_pb_promo_description">
					<h2 class="et_pb_module_header">';
	$content3='	</h2>
					<div>';
	$content4='</div>
				</div>';
	$content5='</div>
	</div>	
</div>	';
	$pie='<!-- --------------------------------< FIN AUZALAN - -->';
	
	
	
	for($i=0;$i<count($array);$i++){
		if (!empty($array[$i]['id'])) {			
			$imagen='';
			$title=$array[$i]['Titulo'];
			$texto='';
			
			if ($array[$i]['Contenido']!=Null){ 
				 $texto=trim($array[$i]['Contenido']);
				// $texto= strip_tags($texto);    /* quito ETIQUETAS HTML AL TEXTO REDUCIDO*/				
			}
			
			if ($array[$i]['Imagen'] != Null){
				$imagen='<img src="' . $array[$i]['Imagen'] . '" alt="" title=""  class="auzalan_mi_sombra"  >';
			}
			$enlace='';
				if ($array[$i]['Url']!=Null){ 
					$enlace='
					<div class="et_pb_button_wrapper"><a target="_blank" class="et_pb_button et_pb_promo_button" href="' . $array[$i]['Url'] .
					'" data-icon="$">Ir a publicación</a></div>';				
			}
			$adjunto='';
				if ($array[$i]['Adjunto']!=Null){ 
					$adjunto='
					<div style="margin-top:5px" class="et_pb_button_wrapper"><a target="_blank"  class="et_pb_button et_pb_promo_button" href="' . $array[$i]['Adjunto'] .
					'" data-icon="$">Descargar Adjunto</a></div>';				
			}
		
			
			
			
			$content =$content . $content1 . $imagen . $content2 . $title . $content3 . $texto . $content4 . $enlace . $adjunto . $content5;
		}
	}
	
	$estilo = ' <link rel="stylesheet" href="'.__AUZALAN_DIR__.'css/auzalan-template-galeriav1-estilo7.css" type="text/css"> ';
	
	if ($error == '' ){
		$devuelvo =$version . $estilo . $content0cab . $content . $content0pie . $pie ;
	}else	{
		$devuelvo =$error;
	}
	
	
	
return ($devuelvo);
}
