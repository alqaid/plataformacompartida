<?php

/**
* 2024-junio (Angel Alcaide)
* @param array Posts
* @return string HTML con los posts formateados
* llamada: 
* 			[plg_auzalan_agenda]
* ejemplos:
* https://aparejastur.es
*/


/*               AUZALAN                       ESTILO el de la propia web        */
function auzalan_agenda_v1( $array, $atts) {
	
	$versionfuncion='_v1';
	
	
	$error= '';
	$contenedorIni='<!-- -------------------------------- plugin  AUZALAN VERSION: ' . __AUZ_VERSION__ .'  -------  function:  auzalan_agenda' . $versionfuncion . '-->';
	$contenedorFin='<!-- fin auzalan_agenda  -->';
	$codigoHTML='';
	for($i=0;$i<count($array);$i++){
		if (!empty($array[$i]['id'])) {	
			$Titulo=$array[$i]['Titulo'];   // ocultamos el título, deben darle negrita ellas
			$FechaAgenda=$array[$i]['FechaAgenda'];
			$Contenido=$array[$i]['Contenido'];
			$codigoHTML=$codigoHTML . '
			<div class="et_pb_with_border et_pb_module et_pb_text et_pb_text_5  et_pb_text_align_left et_pb_bg_layout_dark">
				<div class="et_pb_text_inner">
					<strong>' . $FechaAgenda . '</strong><br>
					' . $Contenido . '
				</div>
			</div>
			';
			/*$codigoHTML=$codigoHTML . '
			<div class="et_pb_with_border et_pb_module et_pb_text et_pb_text_5  et_pb_text_align_left et_pb_bg_layout_dark">
				<div class="et_pb_text_inner">
					<p><strong>' . $FechaAgenda . '</strong><br><strong>' . $Titulo . '</strong></p>
					' . $Contenido . '
				</div>
			</div>
			';*/
		}
	}
	
	
	
	
	$devuelvo= $contenedorIni .  $codigoHTML . $contenedorFin;
	
	
return ($devuelvo);
}
