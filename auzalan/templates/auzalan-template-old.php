<?php

/**
* Devuelve los post de auzalan en formato clásico de auzalan
* @param array Posts
* @return string HTML con los posts formateados
*/
function template_old( $array, $atts) {
	$error= '';
	$contenedorIni='';
	$contenedorFin='';

	if ('columnas_3'==$atts['estilo'] or 'enlaces-3'==$atts['estilo']){
		$maxLength = 300;
		$contenedorIni=$contenedorIni .'<div class="contenedor3">';
		$contenedorFin='</div>';
	}
	
	for($i=0;$i<count($array);$i++){
if (!empty($array[$i]['id'])) {		
		//Estilo de enlaces
		if($atts['estilo'] == 'enlaces-3' or $atts['estilo'] == 'enlaces-1'){
			$cadena5='';
			$cadena51='';
			$cadena6='56';
			if ($array[$i]['Adjunto']!=Null){ 
				$cadena5= '<a href="' . $array[$i]['Adjunto'] . '" target="_blank">';
				$cadena51= '</a>';
			}else if($array[$i]['Url']!=Null){
				$cadena5= '<a href="' . $array[$i]['Url'] . '" target="_blank">';
				$cadena51= '</a>';
			}
			if ($array[$i]['CategoriaPadre']!=Null){ 
				$cadena6=$array[$i]['CategoriaPadre'];
			}
			$cadena6= '<img src="'.__AUZALAN_WEB_DIRECTORIO__.'' . $cadena6 . '.png" class="alineado" alt="imagen">';
			$estilo_interno = ($atts['estilo'] == 'enlaces-3') ? 'columnas_3' : 'columnas_1';
			$cadena1=
				'<div class="auzalan-table' .  $estilo_interno. '">
					<table class="auzalan-table" style="width:100%">
						<tr class="auzalan-table">
							<td class="auzaicon" style="padding:0 0 0 0%">'.$cadena6.'</td>
						<td class="auzalan-table"><h5>'.$cadena5 . $array[$i]['Titulo'] .$cadena51 . '</h5></td>
					  </tr>
					</table>';
			$cadena4='</div>';
			$cadena0 = $cadena0. $cadena1 . $cadena2. $cadena3. $cadena4. ' ';			
			
		}else{
		//Estilo completo
			$cadena5='';
			$cadena51='';
			$cadena6='56';	//CADENA 6 -> ID Tablón de anuncios (cambia si [CategoriaPadre] != NULL)
			if ($array[$i]['Url']!=Null){ 
				$cadena5= '<a href="' . $array[$i]['Url'] . '" target="_blank">';	// CADENA 5 -> URL de [URL]
				$cadena51= '</a>';													//
			}
			if ($array[$i]['CategoriaPadre']!=Null){ 
				$cadena6=$array[$i]['CategoriaPadre'];	//CADENA 6 -> ID Categoría padre (cambia después)
			}
			$cadena6='<img src="'.__AUZALAN_WEB_DIRECTORIO__.'' . $cadena6 . '.png" class="alineado" alt="imagen" width="50px">';	//CADENA 6 -> Imagen de la categoría
			$estiloDiv = ($atts['estilo'] == 'columnas_3') ? 'columnas_3' : 'columnas_1';
			$cadena1='<div class="' . $estiloDiv . '">' . $cadena6 . '<h3 class="titulo">'.$cadena5 . $array[$i]['Titulo'] .$cadena51 . '</h3>';	//CADENA 1 -> Imagen de la categoría + Título
			$cadena2='<img class="redimension" src="' . $array[$i]['Imagen'] . '">';	//   <h4>' .SSSarray[$i]['Categoria'] .'</h4>      CADENA 2 -> Imagen del nodo + Nombre categoría (ELIMINADA EL 30/10/2017)
			$cadena3='<div class="subcadena" style="display:block"></div><div class="oculto" style="display:none"><br>'. $array[$i]['Contenido'] . '<br>' .  $array[$i]['FechaInicio'] .'<br>Etiquetas:'.  $array[$i]['Etiqueta'] . '<br><a href="' . $array[$i]['Adjunto'] . '" target="_blank">' .  $array[$i]['AdjuntoNombre'] . '</a></div>';
			$cadena4='<a href="#" class="abrir">Ver más</a>'; //CADENA 4 -> Enlace 'Ver más'
			$cadena41='<a class="enlace-al-post" href="'.__AUZALAN_POST_VIEWER_PAGE__.'?postid='. $array[$i]['id'].'">Ir a la publicación</a>';
			$cadena0 = $cadena0. $cadena1 . $cadena2. $cadena3. $cadena4. $cadena41.'</div>';			
		}
}else{
		$error= '<!-- sin conexion --> ';
	}

		
	}		
	

	//-------------  hoja de estilos---------------------------------------------------------------------
	$estilo = '<link rel="stylesheet" href="'.__AUZALAN_DIR__.'css/style.css" type="text/css">';
    /*$estilo = '<style>
	
	 .columnas_1{
		min-width: 245px;
		width: 95%;
		display: inline-block;
		background: #ffffff;
		box-shadow:0px 0px 5px 1px #C9C9C9;
		-webkit-box-shadow:2px 2px 1px 5x #C9C9C9;
		-moz-box-shadow:2px 2px 1px 5px #C9C9C9;
		margin: 10px;
		padding: 10px;
	}
	img.alineado{
		float: left;
		margin: 0px 15px 15px 0px;
		width: 40px;

	} 

	img.redimension{
		max-height: 300px;
	}

	.contenedor3{
	  display: -webkit-flex;
	  display: flex;
	  -webkit-flex-flow: row wrap;
	  flex-flow: row wrap;
	  align-items:initial;
	  width: 100%;
	}
	 .columnas_3{
		min-width: 245px;
		width: 95%;
		display: inline-block;
		background: #ffffff;
		box-shadow:0px 0px 5px 1px #C9C9C9;
		-webkit-box-shadow:2px 2px 1px 5x #C9C9C9;
		-moz-box-shadow:2px 2px 1px 5px #C9C9C9;
		margin: 10px;
		padding: 10px;
	}

	.auzalan-table table, th, td, tbody{
		border: 0px;
		margin: 0 0 0 0;
	}
	
	.auzaicon td{
		width: 40px;
		padding: 0 0 0 0;		
		border: 0px;
	}
		
	
	@media (min-width: 700px) {  
	  .columnas_3 {
		width: 45%;	
	  }
	}

	@media (min-width: 1100px) {
	  .columnas_3 { 
		width: 30%; 
		}   
	  }
	</style>';*/
    //------------- scripts -----------------------------------------------------------------------------
	$script = '
	<script>
		jQuery(document).ready(function() {
			
			var max = '.__MAX_LENGTH__.';
			var cadena, subcadena;

			jQuery(".subcadena").each(function() {
				cadena = jQuery(this).parent().children(".oculto").html();
				if(cadena.length > max) {
					subcadena = jQuery(this).parent().children(".oculto").html().substring(0, max);
					jQuery(this).html(subcadena + " ...");
				} else {
					jQuery(this).parent().children(".oculto").css("display","");
					jQuery(this).parent().children(".abrir").css("display","none");
				}
			});

			jQuery(".abrir").click(function(e){
				jQuery(this).parent().children(".oculto").toggle(0, function(){
					if(jQuery(this).parent().children(".oculto").css("display")=="none"){
						subcadena = jQuery(this).parent().children(".oculto").html().substring(0, max);
						jQuery(this).parent().children(".subcadena").html(subcadena + " ...");
						jQuery(this).parent().children(".abrir").html("Ver más");
					} else {
						jQuery(this).parent().children(".subcadena").html("");
						jQuery(this).parent().children(".abrir").html("Ver menos");
					}
					jQuery("html, body").animate({scrollTop: jQuery(this).parent().children(".titulo").offset().top - 130},300);
				});
				e.preventDefault();	
			});
		});
</script>';
 

if ($error == '' ){
		$devuelvo =$estilo . $contenedorIni .' ' . $cadena0. $script. $contenedorFin.' ';
	}else	{
		$devuelvo =$error;
	}

	return ($devuelvo);
}

 