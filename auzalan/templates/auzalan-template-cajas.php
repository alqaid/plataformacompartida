<?php

/**
* Devuelve los post de auzalan en formato cl�sico de auzalan V2.0.0
* @param array Posts
* @return string HTML con los posts formateados
*/
function template_cajas( $array, $atts) {

	$error= '';
	$contenedorIni='';
	$contenedorFin='';
	
	$contenedorIni=$contenedorIni .'<div class="contenedor3">';
	$contenedorFin='</div>';
	$cajas = "";
	
	for($i=0;$i<count($array);$i++){
if (!empty($array[$i]['id'])) {

	$imagen=($array[$i]['Imagen'] != Null) ? $imagen=$array[$i]['Imagen'] : __AUZALAN_DIR__ . 'css/logo.png';
		$url= __AUZALAN_POST_VIEWER_PAGE__ . '?postid='. $array[$i]['id'];
		$categoriaIcono = ($array[$i]['CategoriaPadre'] != Null) ? $categoriaIcono=$array[$i]['CategoriaPadre'] : '56';	
		$cajaB = '
			<div class="auz-columnas-3 auz-out">
				<div class="auz-caja-img">
					<img class="auz-inner-img" src="' . $imagen . '" />			
				</div>			
				<div class="auz-caja-contenido auz-no-shadow auz-contenido-fill">
					<a href="' . $url . '">
						<h4 class="auz_titulo">'.$array[$i]['Titulo'].'</h4>	
					</a>						
				</div>
				<div class="auz-caja-controls">
					<div class="auz-controls-bottom">
						<div class="auz-control auz-control-left">
							<p> '.$array[$i]['FechaInicio'].' </p>
						</div>
						<div class="auz-control">
							<img class="auz-ico-center" src="'.__AUZALAN_WEB_DIRECTORIO__.''.$categoriaIcono.'.png" />
						</div>
						<div class="auz-control auz-control-rigth">
							<a class="enlace-al-post" href="'.$url.'">Ver m&aacutes</a>
						</div>
					</div>						
				</div>
			</div>';
		$cajas = $cajas . $cajaB;
}else{
		$error= '<!-- sin conexion --> ';
	}		 
	}

	//-------------  hoja de estilos---------------------------------------------------------------------
	$estilo = '<link rel="stylesheet" href="'.__AUZALAN_DIR__.'css/style.css" type="text/css">';
   
    //------------- scripts -----------------------------------------------------------------------------
	if ($error == '' ){
		$devuelvo =$estilo . $contenedorIni .' ' . $cajas. $contenedorFin.' ';
	}else	{
		$devuelvo =$error;
	}

	return ($devuelvo);
}

 