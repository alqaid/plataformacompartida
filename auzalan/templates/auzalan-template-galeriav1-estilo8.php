<?php

/**
* Devuelve los post de auzalan en formato galeria de feb-mar 2018 (Angel Alcaide)
* @param array Posts
* @return string HTML con los posts formateados
* llamada: 
* 			[plg_auzalan_galeria estilo="estilox" num_columnas="3" num_nodos_max="20" buscar="novedades"]
* ejemplos:
* http://icolegia.ihabiteformacion.es/auzalan-ejemplos-de-diseno/
*/


/*          ESTILO 8 NEW ALBACETE PORTADA 
					SOLO IMAGENES
      */
function auzalan_template_galeria_v1_estilo8( $array, $atts) {
	$error= '';
	$version ='<!-- -------------------------------- AUZALAN VERSION: '. __AUZ_VERSION__ .' estilo8 -->';
	$content='';
	$content0cab='<div class="auzalan_grid-gallery">';
	$content0pie='</div>';
	$pie='<!-- --------------------------------< FIN AUZALAN - -->';
	
	
	
	for($i=0;$i<count($array);$i++){
		if (!empty($array[$i]['id'])) {			
			$imagen='';
			$title=$array[$i]['Titulo'];
		 
			 
			if ($array[$i]['Imagen'] != Null){
				$imagen='<img class="auzalan_grid-gallery__image" src="' . $array[$i]['Imagen'] . '" alt="" title="' . $title . '"   >';
			}
			$url=__AUZALAN_POST_VIEWER_PAGE__ . '?postid='. $array[$i]['id'];			
			
			$content =$content . '<a class="auzalan_grid-gallery__item" href="' . $url . '">' . $imagen . '</a>';
		}
	}
	
	$estilo = ' <link rel="stylesheet" href="'.__AUZALAN_DIR__.'css/auzalan-template-galeriav1-estilo8.css" type="text/css"> ';
	
	if ($error == '' ){
		$devuelvo =$version . $estilo . $content0cab . $content . $content0pie . $pie ;
	}else	{
		$devuelvo =$error;
	}
	
	
	
	
return ($devuelvo);
}
