<?php

/**
* 2020-septiembre (Angel Alcaide)
* @param array Posts
* @return string HTML con los posts formateados
* llamada: 
* 			[plg_auzalan_galeria bdd="activatie" estilo="estilox" num_columnas="3" num_nodos_max="20"]
* ejemplos:
* http://icolegia.ihabiteformacion.es/auzalan-ejemplos-de-diseno/
*/


/*        formacion                             ESTILO 4        */
function auzalan_activatie_estilo4( $arrayCurso, $atts, $post_id) {

	$error= '';
	$contenedorIni='<!-- -------------------------------- AUZALAN VERSION: ' 
		. __AUZ_VERSION__ .' --><ul class="auz_gallery_v1">';
	$contenedorFin='</ul><!-- --------------------------------< FIN AUZALAN - -->';
	 
	$galeria = "";
	/* IMAGEN */
   
   $array=$arrayCurso['curso'];
	
	for($i=0;$i<count($array);$i++){
       
	 
	 if (!empty($array[$i]['id'])) {
	/*
		ERROR SI INCLUYO ATTS...
	*/
		 
	 if ( $array[$i]['id']==$post_id){
		$imagen='';
		$imagenicono = '<img src="' . __AUZALAN_DIR__ . '/css/icono.png" width="78px"/>';

		if ($array[$i]['imageninterior'] != Null){
				$imagen='<figure><img src="' . $array[$i]['imageninterior'] . '" /></figure>';
		}
		
		/* TITULOS*/
		$titulo=$array[$i]['nombre'];
		
		/* enlaces */
		$enlace_izquierda='';
		
		$enlace_derecha='';
		if ($array[$i]['link']!=Null){ 
				$enlace_derecha='<a href="' . $array[$i]['link'] .'" target="_blank">inscripción</a>';				
		}
		
 
		$contenido='';
		if ($array[$i]['descripcion']!=Null){ 
				 $contenido=trim($array[$i]['descripcion']);
		}
		   
		if ($array[$i]['lugar']!=Null){ $contenido .='<br><b>Lugar</b>: ' .  $array[$i]['lugar'] .'<br>';}
		if ($array[$i]['duracion']!=Null){  $contenido .='<br><b>Duración</b>: ' .  $array[$i]['duracion'] .'<br>';}
		if ($array[$i]['tipo']!=Null){  $contenido .='<br><b>Tipo</b>: ' .  $array[$i]['tipo'] .'<br>';}
		if ($array[$i]['modalidad']!=Null){  $contenido .='<br><b>Modalidad</b>: ' .  $array[$i]['modalidad'] .'<br>';}
		if ($array[$i]['precio_colegiado']!=Null){  $contenido .='<br><b>Precio Colegiado</b>: ' .  $array[$i]['precio_colegiado'] .'<br>';}
		if ($array[$i]['precio_nocolegiado']!=Null){  $contenido .='<br><b>Precio No Colegiado</b>: ' .  $array[$i]['precio_nocolegiado'] .'<br>';}
		if ($array[$i]['fecha_inicio']!=Null){  $contenido .='<br><b>Fecha Inicio</b>: ' .  $array[$i]['fecha_inicio'] .'<br>';}
		if ($array[$i]['fecha_fin']!=Null){  $contenido .='<br><b>Fecha Fin</b>: ' .  $array[$i]['fecha_fin'] .'<br>';}
		if ($array[$i]['horarios']!=Null){  $contenido .='<br><b>Horarios</b>: ' .  $array[$i]['horarios'] .'<br>';}
		
		foreach($array[$i]['areas'] as $arrayAreas){
			if ($arrayAreas['nombre']!=Null){    $contenido .='<b>    Area : </b>' . $arrayAreas['nombre'].'<br>' ;  }
		 }
		 
		 
				
	/*	
		if ($array[$i]['requisitos']!=Null){  $contenido .='<br><b>Requisitos</b>: ' .  $array[$i]['requisitos'] .'<br>';}
		if ($array[$i]['tipoActividad']!=Null){  $contenido .='<br><b>Tipo de actividad</b>: ' .  $array[$i]['tipoActividad'] .'<br>';}
		if ($array[$i]['areaTematica']!=Null){  $contenido .='<br><b>Area temática</b>: ' .  $array[$i]['areaTematica'] .'<br>';}
		if ($array[$i]['lugarCelebracion']!=Null){  $contenido .='<br><b>Lugar de celebración</b>: ' .  $array[$i]['lugarCelebracion'] .'<br>';}
		if ($array[$i]['horasLectivas']!=Null){ $contenido .='<br><b>Horas lectivas</b>: ' .  $array[$i]['horasLectivas'] .'<br>';}


		if ($array[$i]['fechaLimite']!=Null){ $contenido .='<br><b>Fecha límite de Inscripción</b>: ' .  $array[$i]['fechaLimite'] .'<br>';}
		if ($array[$i]['fechaInicio']!=Null){ $contenido .='<br><b>Fecha Inicio del curso</b>: ' .  $array[$i]['fechaInicio'] .'<br>';}
		if ($array[$i]['fechaFin']!=Null){ $contenido .='<br><b>Fecha Fin del curso</b>: ' .  $array[$i]['fechaFin'] .'<br>';}
		
        if ($array[$i]['horario']!=Null){ $contenido .='<br><b>Calendario</b>: ' .  $array[$i]['horario'] .'<br>';}
		
		if(count($array[$i]['precios']) > 0){
			$contenido .='<div class="FormAuz_columnas_1" >';
			$contenido .='</div>';
				if ($array[$i]['precios']['descripcion1']!=Null){ $contenido .='<br><b>' . $array[$i]['precios']['descripcion1'] . '</b>: ' . $array[$i]['precios']['precio1'] .' €<br>';}
				if ($array[$i]['precios']['descripcion2']!=Null){ $contenido .='<br><b>' . $array[$i]['precios']['descripcion2'] . '</b>: ' . $array[$i]['precios']['precio2'] .' €<br>';}
				if ($array[$i]['precios']['descripcion3']!=Null){ $contenido .='<br><b>' . $array[$i]['precios']['descripcion3'] . '</b>: ' . $array[$i]['precios']['precio3'] .' €<br>';}
				if ($array[$i]['precios']['descripcion4']!=Null){ $contenido .='<br><b>' . $array[$i]['precios']['descripcion4'] . '</b>: ' . $array[$i]['precios']['precio4'] .' €<br>';}
				if ($array[$i]['precios']['descripcion5']!=Null){ $contenido .='<br><b>' . $array[$i]['precios']['descripcion5'] . '</b>: ' . $array[$i]['precios']['precio5'] .' €<br>';}
				if ($array[$i]['precios']['descripcion6']!=Null){ $contenido .='<br><b>' . $array[$i]['precios']['descripcion6'] . '</b>: ' . $array[$i]['precios']['precio6'] .' €<br>';}
		}
		if ($array[$i]['subvencionado']!=Null){ $contenido .='<br><b>Curso Subvencionado</b>: ' .  $array[$i]['subvencionado'] .'<br>';}
		 
	*/	 
		 
		 
		/*   
		$adjunto='';
		if ($array[$i]['adjunto'] != Null){
				$adjunto='<a href="' . $array[$i]['adjunto'] . '" target="_blank"/>descargar adjunto</a>';
		}
		$textoampliado = $contenido . '<br>' . $adjunto . '<br>';
		*/
		 
		/* UNIR EL ESTILO ENTERO */
		$galeria .= '
		<li>
		<div class="auz_box">
			' . $imagen . '	
			<div class="contenido">
				<div class="titulo"> 
					<div class="tituloicono">' . $imagenicono . '</div>
					<div class="titulotexto"><h3>' . $titulo . '</h3></div>
				</div>
				<section class="textoampliado">' . $contenido . '</section>				
				<section class="auz_enlace_derecha">' . $enlace_derecha . '</section>
			</div>
		</div>
		</li>
		';
	} 
	}else{
		$error= '<!-- sin conexion --> ';
	 
	}
	}

	//-------------  hoja de estilos---------------------------------------------------------------------
	/* agregamos despues el NUMERO DE COLUMNAS para que tenga prioridad */
	$estilo = '<link rel="stylesheet" href="'.__AUZALAN_DIR__.'css/auzalan-activatie-estilo4.css" type="text/css">';
 
	
	$script = '';
    //------------- scripts -----------------------------------------------------------------------------

	if ($error == '' ){
		$devuelvo =$estilo . $contenedorIni .' ' . $galeria . ' ' . $contenedorFin .' ' . $script . ' ';
	}else	{
		$devuelvo =$error;
	}
	 
return ($devuelvo);
}
