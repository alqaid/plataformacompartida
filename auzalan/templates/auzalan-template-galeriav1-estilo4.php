<?php

/**
* Devuelve los post de auzalan en formato galeria de feb-mar 2018 (Angel Alcaide)
* @param array Posts
* @return string HTML con los posts formateados
* llamada: 
* 			[plg_auzalan_galeria estilo="estilox" num_columnas="3" num_nodos_max="20" buscar="novedades"]
* ejemplos:
* http://icolegia.ihabiteformacion.es/auzalan-ejemplos-de-diseno/
*/


/*                                     ESTILO 3        */
function auzalan_template_galeria_v1_estilo4( $array, $atts) {
	$error= '';
	$contenedorIni='<!-- -------------------------------- AUZALAN VERSION: ' 
		. __AUZ_VERSION__ .' estilo4 --><ul class="auz_gallery_v1">';
	$contenedorFin='</ul><!-- --------------------------------< FIN AUZALAN - -->';
	 
	$galeria = "";
	/* IMAGEN */
	for($i=0;$i<count($array);$i++){
	if (!empty($array[$i]['id'])) {
    
	
		$id='auz_id_' . $array[$i]['id'];
		
		$id_texto_reducido='auz_id_txt_red_' . $array[$i]['id'];
		$id_enlace='auz_id_enlace_' . $array[$i]['id'] ;
		
		$imagen='';
		$categoriaIcono = ($array[$i]['CategoriaPadre'] != Null) ? $categoriaIcono=$array[$i]['CategoriaPadre'] : '56';	
		$imagenicono = '<img src="' . __AUZALAN_WEB_DIRECTORIO__ .$categoriaIcono.'.png" width="78px"/>';
			
		if ($array[$i]['Imagen'] != Null){
				$imagen='<figure><img src="' . $array[$i]['Imagen'] . '" /></figure>';
		}
		
		/* TITULOS*/
		$titulo=$array[$i]['Titulo'];
		
		/* enlaces */
		$enlace_izquierda='';
		
		$enlace_derecha='';
		if ($array[$i]['Url']!=Null){ 
				$enlace_derecha='<a href="' . $array[$i]['Url'] .'" target="_blank">ir a publicación</a>';				
		}
		
		/* TEXTOS */
		/*elimino ademas strip_tags etiquetas html, ver como puedo permitir algunas en http://php.net/manual/es/function.strip-tags.php*/
		 
		 
		$contenido='';
		if ($array[$i]['Contenido']!=Null){ 
				 $contenido=trim($array[$i]['Contenido']);
		}
		 
		/* ADJUNTO */
		$adjunto='';
		if ($array[$i]['Adjunto'] != Null){
				$adjunto='<a href="' . $array[$i]['Adjunto'] . '" target="_blank"/>descargar adjunto</a>';
		}
		$textoampliado = $contenido . '<br>' . $adjunto . '<br>';
		
		
		/* UNIR EL ESTILO ENTERO */
		$galeria .= '
		<li>
		<div class="auz_box">
			' . $imagen . '	
			<div class="contenido">
				<div class="titulo"> 
					<div class="tituloicono">' . $imagenicono . '</div>
					<div class="titulotexto"><h3>' . $titulo . '</h3></div>
				</div>
				<section class="textoampliado">' . $textoampliado . '</section>				
				<section class="auz_enlace_derecha">' . $enlace_derecha . '</section>
			</div>
		</div>
		</li>
		';
	}else{
		$error= '<!-- sin conexion --> ';
	}
	}

	//-------------  hoja de estilos---------------------------------------------------------------------
	/* agregamos despues el NUMERO DE COLUMNAS para que tenga prioridad */
	$estilo = '<link rel="stylesheet" href="'.__AUZALAN_DIR__.'css/auzalan-template-galeriav1-estilo4.css" type="text/css">';
 
	
	$script = '';
    //------------- scripts -----------------------------------------------------------------------------

	if ($error == '' ){
		$devuelvo =$estilo . $contenedorIni .' ' . $galeria . ' ' . $contenedorFin .' ' . $script . ' ';
	}else	{
		$devuelvo =$error;
	}
return ($devuelvo);
}
