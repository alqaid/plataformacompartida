<?php

/**
* Devuelve los post de auzalan en formato galeria de 2024-07-03 (Angel Alcaide)
* @param array Posts
* @return string HTML con los posts formateados
* llamada: 
* 			[plg_auzalan_galeria estilo="estilox" num_columnas="3" num_nodos_max="20" buscar="novedades"]
* ejemplos:
* http://icolegia.ihabiteformacion.es/auzalan-ejemplos-de-diseno/
*/


/*                                     ESTILO ESTILO estilo9AS PERO DE AUZALAN 
										  ESTILOS DIVI DE asturias   */

function f_auzalan_template_AS( $array, $atts) {

// si fallo desbloquear la siguiente linea	
//}  function f_auzalan_template_AS_fallo( $array, $atts) {
	$error= '';
	$contenedorIni='<!-- -------------------------------- AUZALAN VERSION: ' 	   
		. __AUZ_VERSION__ .' ESTILO:estilo9AS (f_auzalan_template_AS)  --><ul class="auz_gallery_v1">';
	$contenedorFin='</ul><!-- --------------------------------< FIN AUZALAN - -->';
	$num_columnas='33.333%';
	if(is_numeric($atts["num_columnas"])) {
			$valor=100/ $atts["num_columnas"];
			$num_columnas=$valor . '%';
	}
	$galeria = "";
	/* IMAGEN */
	for($i=0;$i<count($array);$i++){
	if (!empty($array[$i]['id'])) {
    
	   //
	   //	$id='auz_id_' . $array[$i]['id'];
	   //	
	   //	$id_texto_reducido='auz_id_txt_red_' . $array[$i]['id'];
	   //	$id_enlace='auz_id_enlace_' . $array[$i]['id'] ;
	   //	
	   	$imagen='';
	   	$categoriaIcono = ($array[$i]['CategoriaPadre'] != Null) ? $categoriaIcono=$array[$i]['CategoriaPadre'] : '56';	
	   	$imagen = '<img src="' . __AUZALAN_DIR__ . 'imagen.png"  width="400px" height="300px" class="auzalan_caja_sombra"/>';
	   	//$imagenicono ='';
	   //		
	   	if ($array[$i]['Imagen'] != Null){
	   			$imagen='<img src="' . $array[$i]['Imagen'] . '" width="400px" height="300px" class="auzalan_caja_sombra"/>';
	   	}
	   //	
	   //	/* TITULOS*/
	   //	$titulo=$array[$i]['Titulo'];
	   //	if (strlen($titulo)>__MAX_LENGTH_caracteresTitulo_estilo7__){
	   //		$titulo= substr($titulo,0,__MAX_LENGTH_caracteresTitulo_estilo7__) . '...';
	   //	}
	   //	
	   //	
	   //	$textoreducido='&nbsp;';
	   //	$textoampliado='';
	   //	$contenido='';
	   //	
	   //	/* enlaces */
	   // 	$enlace_izquierda='';
	   // 	$enlace_derecha='';
	   //	//$leermas='<a href="'.__AUZALAN_POST_VIEWER_PAGE__.'?postid='. $array[$i]['id'].'">Leer más</a>';
	   //	//$leermas='<div class="et_pb_button_wrapper"><a target="_blank" class="et_pb_button et_pb_promo_button" href="'.__AUZALAN_POST_VIEWER_PAGE__.'?postid='. $array[$i]['id'].'" data-icon="$">Leer más</a></div>';
	   //	//$leermas='<a class="et_pb_button et_pb_button_0 et_animated et_hover_enabled et_pb_bg_layout_light" href="'.__AUZALAN_POST_VIEWER_PAGE__.'?postid='. $array[$i]['id'].'" data-icon="&#x45;">Leer más</a>';
	    	$leermas='<div class="auzleermas_caja">
	    				<a class="et_pb_button et_pb_button_0 et_pb_bg_layout_dark" 
	    				href="'.__AUZALAN_POST_VIEWER_PAGE__.'?postid='. $array[$i]['id'].'" 
	    				data-icon="&#x45;">Leer más</a>
	    				</div>';
	    	$titulo=$array[$i]['Titulo']; 
	    
	    
	    	$galeria .= '
	    	<li>
	    	<div class="auz_box">' . $imagen . '			
	    		<div class="et_pb_text_inner auzalan_centrar_texto" >' . $titulo . ' <br><br> ' . $leermas . '</div>
	    	</div>
	    	</li>
	    	';
	     
	   }else{
	   	$error= '<!-- sin conexion --> ';
	   }
	}

	//-------------  hoja de estilos---------------------------------------------------------------------
	/* agregamos despues el NUMERO DE COLUMNAS para que tenga prioridad */
	$estilo =  '<style>.auz_gallery_v1 li{width:' . $num_columnas . ';}</style>'; 
	$estilo .= '<link rel="stylesheet" href="'.__AUZALAN_DIR__.'css/auzalan-hojaestilo-AS.css" type="text/css">';
 
	
	$script = '<script src="'.__AUZALAN_DIR__.'js/auzalan-template-galeriav1.js"></script>	';
    //------------- scripts -----------------------------------------------------------------------------

	if ($error == '' ){
		$devuelvo =$estilo . $contenedorIni .' ' . $galeria . ' ' . $contenedorFin .' ' . $script . ' ';
	}else	{
		$devuelvo =$error;
	}
return ($devuelvo);
}



/*                                     ESTILO estilo9AS PERO DE FORMACIÓN 
										  ESTILOS DIVI DE asturias   */

function f_auzalan_formacion_template_AS( $array, $atts) {
	$error= '';
	
	$contenedorIni='<!-- -------------------------------- AUZALAN VERSION: ' 	   
		. __AUZ_VERSION__ .' ESTILO:estilo9AS (f_auzalan_formacion_template_AS) --><ul class="auz_gallery_v1">';
	$contenedorFin='</ul><!-- --------------------------------< FIN AUZALAN - -->';
	$num_columnas='33.333%';
	if(is_numeric($atts["num_columnas"])) {
			$valor=100/ $atts["num_columnas"];
			$num_columnas=$valor . '%';
	}
	$galeria = "";
	for($i=0;$i<count($array);$i++){
     $mostrar=1;
	 if (!empty($array[$i]['idPb'])) {
	 
	 if (!empty($atts["colegio_excluye"])) {
	 if(is_numeric($atts["colegio_excluye"])) {
		  if($atts["colegio_excluye"]==$array[$i]['CidO']) {
			$mostrar=0;			
		  }	
	 } }
	 if (!empty($atts["colegio"])) {
	 if(is_numeric($atts["colegio"])) {
		  if($atts["colegio"]==$array[$i]['CidO']) {
			$mostrar=1;
			}else{
			$mostrar=0;
		  }	
	 } }
     if ( $mostrar==1){
		$titulo=mb_strtoupper($array[$i]['titulo']);
		
		if (strlen($titulo)>__MAX_LENGTH_caracteresTitulo_estilo7__){
			$titulo= substr($titulo,0,__MAX_LENGTH_caracteresTitulo_estilo7__) . '...';
		}
		
		$imagen='';		
		$url='';
		
		$leermas='<div class="auzleermas_caja">
					<a class="et_pb_button et_pb_button_0 et_pb_bg_layout_dark auzleermas_caja_enlace" 
					href="'.__FORMACION_AUZALAN_POST_VIEWER_PAGE__.'?postid='. $array[$i]['idPb'].'"  
					data-icon="&#x45;">Leer más</a>
					</div>';
		
		$imagen = '<img src="' . __AUZALAN_DIR__ . 'imagen.png"  width="400px" height="300px" class="auzalan_caja_sombra"/>';
		if ($array[$i]['imagen'] != Null){
				$imagen='<img src="' . $array[$i]['imagen'] . '" width="400px" height="300px" class="auzalan_caja_sombra"/>';
		}
		
		/* UNIR EL ESTILO ENTERO */
		$galeria .= '
		<li>
		<div class="auz_box">' . $imagen . '			
			<div class="et_pb_text_inner auzalan_centrar_texto" >' . $titulo . ' <br><br> ' . $leermas . '</div>
		</div>
		</li>
		';
		 
	 }}else{
		$error= '<!-- sin conexion --> ';
	}
	} // end FOR
	
	//-------------  hoja de estilos---------------------------------------------------------------------
	/* agregamos despues el NUMERO DE COLUMNAS para que tenga prioridad */
	$estilo =  '<style>.auz_gallery_v1 li{width:' . $num_columnas . ';}</style>'; 
	$estilo .= '<link rel="stylesheet" href="'.__AUZALAN_DIR__.'css/auzalan-hojaestilo-AS.css" type="text/css">';
 
	$script = '<script src="'.__AUZALAN_DIR__.'js/auzalan-template-galeriav1.js"></script>	';
	
	if ($error == '' ){
		$devuelvo =$estilo . $contenedorIni .' ' . $galeria . ' ' . $contenedorFin .' ' . $script . ' ';
	}else	{
		$devuelvo =$error;
	}


return ($devuelvo  );
}	
	
	
	
/*                                     ESTILO estilo9AS PERO DE activatie 
										  ESTILOS DIVI DE asturias   */

function f_auzalan_activatie_template_AS( $arrayCurso, $atts) {
	$error= '';
	
	$contenedorIni='<!-- -------------------------------- AUZALAN VERSION: ' 	   
		. __AUZ_VERSION__ .' ESTILO:estilo9AS (f_auzalan_activatie_template_AS) --><ul class="auz_gallery_v1">';
	$contenedorFin='</ul><!-- --------------------------------< FIN AUZALAN - -->';
	$num_columnas='33.333%';
	if(is_numeric($atts["num_columnas"])) {
			$valor=100/ $atts["num_columnas"];
			$num_columnas=$valor . '%';
	}
	$galeria = "";
	
	$array=$arrayCurso['curso'];
	for($i=0;$i<count($array);$i++){
 
	 if (!empty($array[$i]['id'])) {
	  
   
		$titulo=mb_strtoupper($array[$i]['nombre']);
		
		if (strlen($titulo)>__MAX_LENGTH_caracteresTitulo_estilo7__){
			$titulo= substr($titulo,0,__MAX_LENGTH_caracteresTitulo_estilo7__) . '...';
		}
		
		$imagen='';		
		$url='';
		
		$leermas='<div class="auzleermas_caja">
					<a class="et_pb_button et_pb_button_0 et_pb_bg_layout_dark auzleermas_caja_enlace" 
					href="'.__AUZALAN_ACTIVATIE_POST_VIEWER_PAGE__.'?postid='. $array[$i]['id'].'"  
					data-icon="&#x45;">Leer más</a>
					</div>';
		
		$imagen = '<img src="' . __AUZALAN_DIR__ . 'imagen.png"  width="400px" height="300px" class="auzalan_caja_sombra"/>';
		if ($array[$i]['imagen'] != Null){
				$imagen='<img src="' . $array[$i]['imagen'] . '" width="400px" height="300px" class="auzalan_caja_sombra"/>';
		}
		
		/* UNIR EL ESTILO ENTERO */
		$galeria .= '
		<li>
		<div class="auz_box">' . $imagen . '			
			<div class="et_pb_text_inner auzalan_centrar_texto" >' . $titulo . ' <br><br> ' . $leermas . '</div>
		</div>
		</li>
		';
		 
	 }else{
		$error= '<!-- sin conexion --> ';
	}
	} // end FOR
	
	//-------------  hoja de estilos---------------------------------------------------------------------
	/* agregamos despues el NUMERO DE COLUMNAS para que tenga prioridad */
	$estilo =  '<style>.auz_gallery_v1 li{width:' . $num_columnas . ';}</style>'; 
	$estilo .= '<link rel="stylesheet" href="'.__AUZALAN_DIR__.'css/auzalan-hojaestilo-AS.css" type="text/css">';
 
	$script = '<script src="'.__AUZALAN_DIR__.'js/auzalan-template-galeriav1.js"></script>	';
	
	if ($error == '' ){
		$devuelvo =$estilo . $contenedorIni .' ' . $galeria . ' ' . $contenedorFin .' ' . $script . ' ';
	}else	{
		$devuelvo =$error;
	}


return ($devuelvo  );
}	
	

/*                                     ESTILO estilo9AS PERO DE activatie  DETALLE
										  ESTILOS DIVI DE asturias   */
										  
function f_auzalan_activatie_template_AS_detalle($arrayCurso, $atts, $post_id) {
 
	$error= '';
	$contenedorIni='<!-- -------------------------------- AUZALAN VERSION: ' 
		. __AUZ_VERSION__ .' --><ul class="auz_gallery_v1">';
	$contenedorFin='</ul><!-- --------------------------------< FIN AUZALAN - -->';
	 
	$galeria = "";
	/* IMAGEN */
	
	$array=$arrayCurso['curso'];
	for($i=0;$i<count($array);$i++){
    if (!empty($array[$i]['id'])) {
	if ( $array[$i]['id']==$post_id){
 
		$imagen='';
		 
		if ($array[$i]['imageninterior'] != Null){
				$imagen=$array[$i]['imageninterior'];
		}
		
		/* TITULOS*/
		$titulo=$array[$i]['nombre'];
		
		
		
		/* TEXTOS */
		$contenido='<b>' . mb_strtoupper($titulo) . '</b><br><br>';
		if ($array[$i]['descripcion']!=Null){ 
				 $contenido .=trim($array[$i]['descripcion']);
		}
		if ($array[$i]['lugar']!=Null){ $contenido .='<br><b>Lugar</b>: ' .  $array[$i]['lugar'] .'<br>';}
		if ($array[$i]['duracion']!=Null){ $contenido .='<br><b>Duración</b>: ' .  $array[$i]['duracion'] .'<br>';}
		if ($array[$i]['tipo']!=Null){ $contenido .='<br><b>Tipo</b>: ' .  $array[$i]['tipo'] .'<br>';}
		if ($array[$i]['modalidad']!=Null){ $contenido .='<br><b>Modalidad</b>: ' .  $array[$i]['modalidad'] .'<br>';}
		if ($array[$i]['precio_colegiado']!=Null){ $contenido .='<br><b>Precio Colegiado</b>: ' .  $array[$i]['precio_colegiado'] .'<br>';}
		if ($array[$i]['precio_nocolegiado']!=Null){ $contenido .='<br><b>Precio No Colegiado</b>: ' .  $array[$i]['precio_nocolegiado'] .'<br>';}
		if ($array[$i]['fecha_inicio']!=Null){ $contenido .='<br><b>Fecha Inicio</b>: ' .  $array[$i]['fecha_inicio'] .'<br>';}
		if ($array[$i]['fecha_fin']!=Null){ $contenido .='<br><b>Fecha Fin</b>: ' .  $array[$i]['fecha_fin'] .'<br>';}
		if ($array[$i]['horarios']!=Null){ $contenido .='<br><b>Horarios</b>: ' .  $array[$i]['horarios'] .'<br>';}
		
		foreach($array[$i]['areas'] as $arrayAreas){
			if ($arrayAreas['nombre']!=Null){    $contenido .='<b>    Area : </b>' . $arrayAreas['nombre'].'<br>' ;  }
		 }
		 
		$enlace='';
				if ($array[$i]['link']!=Null){ 
					$enlace='<div >
					<a class="et_pb_button et_pb_button_0 et_pb_bg_layout_dark auzleermas_caja_enlace" 
					href="' . $array[$i]['link'] .'"  
					data-icon="&#x45;" target="_blank">Inscripción</a>
					</div>';
					
			}
	/*	$adjunto='';
			if ($array[$i]['adjunto']!=Null){ 
				$adjunto='
				<div style="margin-top:5px" class="et_pb_button_wrapper"><a target="_blank"  class="et_pb_button et_pb_promo_button" href="' . $array[$i]['adjunto'] .
				'" data-icon="$">Descargar Adjunto</a></div>';				
		}*/
		
		
		$contenido=$contenido . '<br><br>' . $adjunto  . $enlace;	

		
		/* UNIR EL ESTILO ENTERO */
		$galeria .= '
	 
<div class="et_pb_row et_pb_row_1 et_pb_gutters3">	

	<div class="et_pb_module et_pb_image et_pb_image_0">				
				<span class="et_pb_image_wrap has-box-shadow-overlay"><div class="box-shadow-overlay"></div><img  src="' . $imagen . '	
			" alt="" title=""  class="auzalan_mi_sombra" /></span>
	</div>
	<div class="et_pb_with_border et_pb_module et_pb_cta_0 et_hover_enabled et_pb_promo  et_pb_text_align_left et_pb_bg_layout_light et_pb_no_bg">' . $contenido . '</div>
</div>';
	} 
	}else{
		$error= '<!-- sin conexion --> ';
	 
	} // fin FOR
	}

	//-------------  hoja de estilos---------------------------------------------------------------------
	/* agregamos despues el NUMERO DE COLUMNAS para que tenga prioridad */
	$estilo = '<link rel="stylesheet" href="'.__AUZALAN_DIR__.'css/auzalan-hojaestilo-AS.css" type="text/css">';
 
	
	$script = '';
    //------------- scripts -----------------------------------------------------------------------------



	if ($error == '' ){
		$devuelvo =$estilo . $contenedorIni .' ' . $galeria . ' ' . $contenedorFin .' ' . $script . ' ';
	}else	{
		$devuelvo =$error;
	}
return ($devuelvo);
}



/*                                     ESTILO estilo9AS PERO DE FORMACIÓN  DETALLE
										  ESTILOS DIVI DE asturias   */
function f_auzalan_formacion_template_AS_detalle( $array, $atts,$idioma ) {
 
	$error= '';
	$contenedorIni='<!-- -------------------------------- AUZALAN VERSION: ' 
		. __AUZ_VERSION__ .' --><ul class="auz_gallery_v1">';
	$contenedorFin='</ul><!-- --------------------------------< FIN AUZALAN - -->';
	 
	$galeria = "";
	/* IMAGEN */
	
	
	// ----- idioma ----------------- ----------------- ----------------- -----------------
	
	$lng_Objetivos="Objetivos";
	$lng_Programa="Programa";
	$lng_Requisitos="Requisitos";
	$lng_TipoActividad="Tipo de actividad";
	$lng_AreaTematica="Area temática";
	$lng_Modalidad="Modalidad";
	$lng_LugarCelebracion="Lugar de celebración";
	$lng_HorasLectivas="Horas lectivas";
	$lng_FlimiteInscripcion="Fecha límite de Inscripción";
	$lng_FlimiteInicio="Fecha inicio del curso";
	$lng_FlimiteFin="Fecha fin del curso";
	$lng_Calendario="Calendario";
	$lng_CursoSubvencionado ="Curso Subvencionado";
	$lng_DescargarAdjunto ="descargar adjunto";
	$lng_verMas="Ver más";
	$lng_verMenos="Ver menos";
	$lng_verPublicacion="Ver publicación";
	$lng_verVideo="Ver vídeo";
	$lng_inscripcion="inscripcion";
	
		if ($idioma == 'idioma2')  {	
				 $lng_Objetivos="Helburuak";
				 $lng_Programa="Programa";
				 $lng_Requisitos="Baldintzak";
				 $lng_TipoActividad="Jarduera mota";
				 $lng_AreaTematica="Gaikako arloa";
				 $lng_Modalidad="Modalidad";
				 $lng_LugarCelebracion="Ospakizun lekua";
				 $lng_HorasLectivas="Irakaskuntza orduak";
				 $lng_FlimiteInscripcion="Izena emateko epea";
				 $lng_FlimiteInicio="Ikastaroaren hasiera data";
				 $lng_FlimiteFin="Ikastaroaren amaiera data";
				 $lng_CursoSubvencionado ="Diruz lagundutako ikastaroa";
				 $lng_Calendario="Egutegia";
				 $lng_DescargarAdjunto ="eranskina deskargatu";
				 $lng_verMas="Gehiago ikusi";
				 $lng_verMenos="Gutxiago ikusi";
				 $lng_verPublicacion="Ikusi argitalpena";
				 $lng_verVideo="Ikusi bideoa";
				 $lng_inscripcion="inskripzioa";
		}
	// ----- fin idioma ----------------- ----------------- ----------------- ----------------- ----------------- -----------------
	
	
	
	
	for($i=0;$i<count($array);$i++){
     $mostrar=1;
	 if (!empty($array[$i]['idPb'])) {
	/*
		ERROR SI INCLUYO ATTS...
	*/
	 if ( $mostrar==1){
		$imagen='';
		$textotiva='';
		$imagenicono = '<img src="' . __AUZALAN_DIR__ . '/css/icono.png" width="78px"/>';

		if ($array[$i]['imagen'] != Null){
				$imagen=$array[$i]['imagen'];
		}
		
		/* TITULOS*/
		$titulo=$array[$i]['titulo'];
		
		
		
		/* TEXTOS */
		$contenido='<b>' . mb_strtoupper($titulo) . '</b><br><br>';
		if ($array[$i]['descripcion']!=Null){ 
				 $contenido .=trim($array[$i]['descripcion']);
		}
		if ($array[$i]['objetivos']!=Null){ $contenido .='<br><b>' . $lng_Objetivos . '</b>: ' .  $array[$i]['objetivos'] .'<br>';}
		if ($array[$i]['programa']!=Null){  $contenido .='<br><b>' . $lng_Programa . '</b>: ' .  $array[$i]['programa'] .'<br>';}
		
		if ($array[$i]['requisitos']!=Null){  $contenido .='<br><b>' . $lng_Requisitos . '</b>: ' .  $array[$i]['requisitos'] .'<br>';}
		if ($array[$i]['tipoActividad']!=Null){  $contenido .='<br><b>' . $lng_TipoActividad . '</b>: ' .  $array[$i]['tipoActividad'] .'<br>';}
		if ($array[$i]['modalidad']!=Null){  
			$contenido .='<br><b>' . $lng_Modalidad . '</b>: ' .  $array[$i]['modalidad'] .'<br>';
			if ($array[$i]['modalidad']=='e-Learning') { $textotiva=" (+ 21 % IVA)";}
		
		}
		if ($array[$i]['areaTematica']!=Null){  $contenido .='<br><b>' . $lng_AreaTematica . '</b>: ' .  $array[$i]['areaTematica'] .'<br>';}
		if ($array[$i]['lugarCelebracion']!=Null){  $contenido .='<br><b>' . $lng_LugarCelebracion . '</b>: ' .  $array[$i]['lugarCelebracion'] .'<br>';}
		if ($array[$i]['horasLectivas']!=Null){ $contenido .='<br><b>' . $lng_HorasLectivas . '</b>: ' .  $array[$i]['horasLectivas'] .'<br>';}


		if ($array[$i]['fechaLimite']!=Null){ $contenido .='<br><b>' . $lng_FlimiteInscripcion . '</b>: ' .  $array[$i]['fechaLimite'] .'<br>';}
		if ($array[$i]['fechaInicio']!=Null){ $contenido .='<br><b>' . $lng_FlimiteInicio . '</b>: ' .  $array[$i]['fechaInicio'] .'<br>';}
		if ($array[$i]['fechaFin']!=Null){ $contenido .='<br><b>' . $lng_FlimiteFin . '</b>: ' .  $array[$i]['fechaFin'] .'<br>';}
		
        if ($array[$i]['horario']!=Null){ $contenido .='<br><b>' . $lng_Calendario . '</b>: ' .  $array[$i]['horario'] .'<br>';}
		
		if(count($array[$i]['precios']) > 0){
			$contenido .='<div class="FormAuz_columnas_1" >';
			$contenido .='</div>';
				if ($array[$i]['precios']['descripcion1']!=Null){ $contenido .='<br><b>' . $array[$i]['precios']['descripcion1'] . '</b>: ' . $array[$i]['precios']['precio1'] .' €'.$textotiva.'<br>';}
				if ($array[$i]['precios']['descripcion2']!=Null){ $contenido .='<br><b>' . $array[$i]['precios']['descripcion2'] . '</b>: ' . $array[$i]['precios']['precio2'] .' €'.$textotiva.'<br>';}
				if ($array[$i]['precios']['descripcion3']!=Null){ $contenido .='<br><b>' . $array[$i]['precios']['descripcion3'] . '</b>: ' . $array[$i]['precios']['precio3'] .' €'.$textotiva.'<br>';}
				if ($array[$i]['precios']['descripcion4']!=Null){ $contenido .='<br><b>' . $array[$i]['precios']['descripcion4'] . '</b>: ' . $array[$i]['precios']['precio4'] .' €'.$textotiva.'<br>';}
				if ($array[$i]['precios']['descripcion5']!=Null){ $contenido .='<br><b>' . $array[$i]['precios']['descripcion5'] . '</b>: ' . $array[$i]['precios']['precio5'] .' €'.$textotiva.'<br>';}
				if ($array[$i]['precios']['descripcion6']!=Null){ $contenido .='<br><b>' . $array[$i]['precios']['descripcion6'] . '</b>: ' . $array[$i]['precios']['precio6'] .' €'.$textotiva.'<br>';}
		}
		if ($array[$i]['subvencionado']!=Null){ $contenido .='<br><b>' . $lng_CursoSubvencionado . '</b>: ' .  $array[$i]['subvencionado'] .'<br>';}
		 
		 
		 
		 
		 
		$coma='';
		$enlace='';		
		$adjunto='';
		
		if ($array[$i]['adjunto']!=Null){ 
				/*$adjunto='
				 <div style="margin-bottom:15px" class="et_pb_button_wrapper"><a   target="_blank"  class="et_pb_button et_pb_promo_button" href="' . $array[$i]['adjunto'] .
				'" data-icon="$">Descargar Adjunto</a></div>';*/

				$adjunto= $coma . '<div >
					<a class="et_pb_button et_pb_button_0 et_pb_bg_layout_dark auzleermas_caja_enlace" 
					href="' . $array[$i]['adjunto'] .'"  
					data-icon="&#x45;"  target="_blank">Descargar Adjunto</a>
					</div>';
				$coma='<br>';						
		} 
		
		if ($array[$i]['url']!=Null){ 
				/*	$enlace='
					<div style="margin-bottom:15px" class="et_pb_button_wrapper"><a target="_blank" class="et_pb_button et_pb_promo_button" href="' . $array[$i]['url'] .
					'" data-icon="$">Inscripción</a></div>';	*/


				$enlace=$coma . '<div >
					<a class="et_pb_button et_pb_button_0 et_pb_bg_layout_dark auzleermas_caja_enlace" 
					href="' . $array[$i]['url'] .'"  
					data-icon="&#x45;"  target="_blank">Inscripción</a>
					</div>';

				
			}
		 
			
	  
		
		$contenido=$contenido . '<br><br>' . $adjunto  . $enlace;	

		
		/* UNIR EL ESTILO ENTERO */
		$galeria .= '		
<div class="et_pb_row et_pb_row_1 et_pb_gutters3">	

	<div class="et_pb_module et_pb_image et_pb_image_0">				
				<span class="et_pb_image_wrap has-box-shadow-overlay"><div class="box-shadow-overlay"></div><img  src="' . $imagen . '	
			" alt="" title=""  class="auzalan_mi_sombra" /></span>
	</div>
	<div class="et_pb_with_border et_pb_module et_pb_cta_0 et_hover_enabled et_pb_promo  et_pb_text_align_left et_pb_bg_layout_light et_pb_no_bg">' . $contenido . '</div>
</div>';
	} 
	}else{
		$error= '<!-- sin conexion --> ';
	 
	}
	}

	//-------------  hoja de estilos---------------------------------------------------------------------
	/* agregamos despues el NUMERO DE COLUMNAS para que tenga prioridad */
	$estilo = '<link rel="stylesheet" href="'.__AUZALAN_DIR__.'css/auzalan-hojaestilo-AS.css" type="text/css">';
 
	
	$script = '';
    //------------- scripts -----------------------------------------------------------------------------



	if ($error == '' ){
		$devuelvo =$estilo . $contenedorIni .' ' . $galeria . ' ' . $contenedorFin .' ' . $script . ' ';
	}else	{
		$devuelvo =$error;
	}
return ($devuelvo);
}


/*                                     ESTILO estilo9AS PERO DE AUZALAN  DETALLE
										  ESTILOS DIVI DE asturias   */
function f_auzalan_template_AS_detalle( $array, $atts) {
	$error= '';
	$contenedorIni='<!-- -------------------------------- AUZALAN VERSION: ' 
		. __AUZ_VERSION__ .' --><ul class="auz_gallery_v1">';
	$contenedorFin='</ul><!-- --------------------------------< FIN AUZALAN - -->';
	 
	$galeria = "";
	$titulo="";
	$contenido="";
	/* IMAGEN */
	
for($i=0;$i<count($array);$i++){
	if (!empty($array[$i]['id'])) {
 
		$imagen = __AUZALAN_DIR__ . 'imagen.png';
		if ($array[$i]['Imagen'] != Null){
				$imagen= $array[$i]['Imagen'];
		}
			
		
		/* TITULOS*/
		if ($array[$i]['Titulo'] != Null){
			$titulo=$array[$i]['Titulo'];
			$contenido='<b>' . mb_strtoupper($titulo) . '</b><br><br>';
		}
		if ($array[$i]['Contenido'] != Null){			
			$contenido=$contenido . $array[$i]['Contenido'];			
		}
		 
		$coma=""; 
		$enlace='';
				if ($array[$i]['Url']!=Null){ 
					$enlace='<div >
					<a class="et_pb_button et_pb_button_0 et_pb_bg_layout_dark auzleermas_caja_enlace" 
					href="' . $array[$i]['Url'] .'"  
					data-icon="&#x45;" target="_blank">Ver enlace</a>
					</div>';
					$coma='<br>';	
			}
		$adjunto='';
				if ($array[$i]['Adjunto']!=Null){ 
					$adjunto= $coma . '<div >
					<a class="et_pb_button et_pb_button_0 et_pb_bg_layout_dark auzleermas_caja_enlace" 
					href="' . $array[$i]['Adjunto'] .'"  
					data-icon="&#x45;" target="_blank">Descargar Adjunto</a>
					</div>';
				 		
					
			}
	 
		
		
		$contenido=$contenido . '<br><br>' . $enlace   .  $adjunto;	

		
		/* UNIR EL ESTILO ENTERO */
		$galeria .= '
	 
<div class="et_pb_row et_pb_row_1 et_pb_gutters3">	

	<div class="et_pb_module et_pb_image et_pb_image_0">				
				<span class="et_pb_image_wrap has-box-shadow-overlay"><div class="box-shadow-overlay"></div><img  src="' . $imagen . '	
			" alt="" title=""  class="auzalan_mi_sombra" /></span>
	</div>
	<div class="et_pb_with_border et_pb_module et_pb_cta_0 et_hover_enabled et_pb_promo  et_pb_text_align_left et_pb_bg_layout_light et_pb_no_bg">' . $contenido . '</div>
</div>';
	  
	}else{
		$error= '<!-- sin conexion --> ';	 
	} 
	
}// fin FOR

	//-------------  hoja de estilos---------------------------------------------------------------------
	/* agregamos despues el NUMERO DE COLUMNAS para que tenga prioridad */
	$estilo = '<link rel="stylesheet" href="'.__AUZALAN_DIR__.'css/auzalan-hojaestilo-AS.css" type="text/css">';
 
	
	$script = '';
    //------------- scripts -----------------------------------------------------------------------------



	if ($error == '' ){
		$devuelvo =$estilo . $contenedorIni .' ' . $galeria . ' ' . $contenedorFin .' ' . $script . ' ';
	}else	{
		$devuelvo =$error;
	}
		
	
return ($devuelvo);
}
